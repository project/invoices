<?php
/**
 * Author: Bernard Szlachta
 * Company: NobleProg Limited
 * URL: www.nobleprog.co.uk
 * bernard.szlachta (at) NobleProg.co.uk
 */


/**
 * Convert date from ISO (yyyy-mm-ddT00:00:00) to unixtime (used date module)
 * @param date in ISO format
 * @return date in unix format
 */
function conv_date_iso2unix($date) {
    if (module_exists('date') && file_exists(drupal_get_path('module', 'date_api').'/date.inc')) {
	include_once(drupal_get_path('module', 'date_api') .'/date.inc');
	return date_iso2unix($date);
    } else {
	drupal_set_message(__FUNCTION__.'(): Error! Check your date module!','error');
	return NULL;
    }
}

function invoices_validate() {
    $result = db_query("SELECT nid FROM {node} WHERE type = 'invoice'");
    while ( ($nid = db_fetch_object($result)) ? $nids[] = $nid : ''); // save selected nodes to array

    // FIRST STEP VALIDATION (check if numbers and dates are not empty)
    $invoices = array();
    $maxnr = 0;
    foreach ($nids as $nid) {
	if (is_numeric($nid->nid)) {
	    $node = node_load($nid->nid);
	    $nr = isset($node->field_invoice_number[0]) ? implode('',$node->field_invoice_number[0]) : NULL;
	    $issue_date = isset($node->field_issue_date[0]) ? conv_date_iso2unix($node->field_issue_date[0]['value']) : NULL;
	    $nlink = l('node/'.$node->nid,'node/'.$node->nid).' '.l('(edit)','node/'.$node->nid.'/edit');
	    if (empty($nr)) {
		drupal_set_message(t('Empty invoice number! - ').$nlink);
	    } else if (!is_numeric($nr)) {
		drupal_set_message(t('Invoice number is not numeric! - ')." is '$nr' - ".$nlink);
	    } else {
		if (!isset($invoices[$nr])) {
		    $invoices[$nr] = Array($node->nid, $issue_date); // add correct invoices to the list for the second validation
		} else {
		    $nid = $invoices[$nr][0]; // get duplicated nid
		    $nlink2 = l('node/'.$nid,'node/'.$nid).' '.l('(edit)','node/'.$nid.'/edit');
		    drupal_set_message(t('Duplicated invoices! - ')." no:'$nr' - ".$nlink.' conflicting with '.$nlink2);
		}
		if ($nr>$maxnr) $maxnr = $nr; // update nr of last invoice
	    }
	    if (empty($date)) {
		drupal_set_message(t('Empty issue date in invoice! - ').$nlink);
	    } else if (!is_string($nr)) {
		drupal_set_message(t('Invalid date of issue in invoice! - ')." is '$nr' - ".$nlink);
	    }
	}
    }

    // SECOND STEP VALIDATION (check order of invoices and correct order of dates)
    $currdate = $invoices[1][1]; // starting unix time
    $lastnr = 0; // last checked invoice
    for ($nr = 1; $nr<$maxnr+1; $nr++) { // check from number 1 to last invoice
	if (!isset($invoices[$nr])) { // if there is no such invoice
	    drupal_set_message(t('There is no invoice nr')." = $nr");
	} else {
	    $nid = $invoices[$nr][0];
	    $issue_date = !empty($invoices[$nr][1]) ? $invoices[$nr][1] : $currdate;
	    if ($issue_date<$currdate) { // check if date time is after than date of last checked invoice
		$nlink = l('node/'.$nid,'node/'.$nid).' '.l('(edit)','node/'.$nid.'/edit');
		drupal_set_message("Invoice nr $nr has date before than invoice nr $lastnr! - ".$nlink);
		$lastnr = $nr; // save this invoice as last 
	    }
	}
    }
}

/**
 * Return last number of invoice from database
 *
 * @param
 * @return integer Last id number of invoice
 */ 

function invoices_get_last_id_from_db() {
    $res = db_result(db_query("SELECT MAX(field_invoice_number_value) FROM {content_type_invoice}"));
    if (!is_numeric($res)) {
    	$res = 0;
	drupal_set_message(__FUNCTION__."(): Can't find last invoice number!",'error');
    }
    return $res;
}


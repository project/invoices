<?php
/**
 * Author: Bernard Szlachta
 * Company: NobleProg Limited
 * URL: www.nobleprog.co.uk
 * bernard.szlachta (at) NobleProg.co.uk
 */

require_once('invoice_statistics.inc');
require_once('invoices_views.inc');
require_once('func.inc.php');
/**
 * Implementation of hook_menu()
 *
 * @param $may_cache A boolean indicating whether cacheable menu items should be returned.
 */
function invoices_menu($may_cache) {
  $items = array();
  if ($may_cache) {
    $access = user_access('administer site configuration');
    $items[] = array(
      'path' => 'admin/nobleprog/invoices',
      'title' => t('Validate Invoices'),
      'callback' => 'drupal_get_form',
      'callback arguments' => Array('invoices_settings_get_form'),
      'description' => t('Validation of invoices'),
      'type'     => MENU_NORMAL_ITEM,
      'access' => $access
    );
        $items[] = array(
      'path' => 'invoice_stats',
      'title' => t('Invoices Information'),
      'callback' => 'invoices_landing_page',
      'description' => t('Invoices Information, summarises, etc.'),
      'type'     => MENU_CALLBACK,
      'access' => user_access('view invoice statistics'),
    );
    
  }

  return $items;
}

function invoices_perm(){
	return array('view invoice statistics');
}
/**
 * Menu callback; handles pages for creating and editing URL aliases.
 */
function invoices_settings_get_form() {
    $form['invoices_validation'] = array (
	    '#type' => 'fieldset',
	    '#title' => t("Invoices validation")
	    );
    $form['invoices_validation']['check_validation'] = array (
	    '#type' => 'submit',
	    '#value' => t("Validate Invoices")
	    );
  return $form;
}


/**
 * Implementation of submit form or something:)
 */
function invoices_settings_get_form_submit($form_id, $form_values) {
    switch ($form_values['op']) {
	case t("Validate Invoices"): {
	    invoices_validate();
	    break;
	}
    }
}

/**
 * Implementation of hook_form_alter().
 */
function invoices_form_alter($form_id, &$form) {
    if ($form_id == 'invoice_node_form') {
		$nid = $form['#node']->nid;
		if (empty($nid) || !is_numeric($nid)) { // insert new number of invoice if nid of node doesn't exist
		    
		    $inv_no = variable_get('invoice_next_id',invoices_get_last_id_from_db()+1); 
		    // get last invoice id from variable (or from db if var dosen't exist)
		    
		    $inv_pf_no = variable_get('invoice_next_pf_id',1); 
		    // get last pro-forma invoice id from variable (or from db if var dosen't exist)

		    if($form['#post']['field_pro_forma']['keys'] == 'yes'){
		    	$form['field_invoice_number'][0]['value']['#value'] = $inv_pf_no; 	
		    } else {
		    	$form['field_invoice_number'][0]['value']['#value'] = $inv_no;
		    }
		    
		    
		    //$form['field_invoice_number'][0]['value']['#attributes']['disabled'] = 'true'; // dissallow editing this component
		    
		    $form['field_invoice_number'][0]['value']['#suffix'] = t('Invoice number auto-generated successfully.'); // add suffix description
		}
    }

	
	if ($form_id == 'invoice_item_node_form') {
		//Set the parent invoice
		if(!isset($form['field_parent_invoice']['nids']['#default_value']['0'])){
			$form['field_parent_invoice']['nids']['#default_value']['0'] = arg(3);
		}
	}
	
}

/**
 * hook_nodeapi Implementation
 */
function invoices_nodeapi(&$node, $op, $teaser = null, $page = null) {
	global $user;

	/**
	 * Injects extra stuff to an invoice
	 */		
	if ($node->type == 'invoice' and $op == 'view') {
		$path = drupal_get_path('module', 'invoices').'/'.'invoices.css';
		drupal_add_css($path);

		//Inject bank details
		if (!empty($node->field_bank_details[0]['nid']) && is_numeric($node->field_bank_details[0]['nid'])) {
		    $node_bank_account = node_load($node->field_bank_details[0]['nid']);
		    $node->content['bank_account'] = array (
			'#value' => theme('bank_account',$node_bank_account ),
			'#title' => 'Bank details',
			'#type' => 'fieldset',
			'#weight' => 9,
			'#attributes' => array( 'class' => 'bank-details'),
		    );	
		}
		
		//Inject invoice items
		$view = views_get_view('invoice_items');	
		$invoice_items = views_build_view('embed', $view, array($node->nid), false, 0, false, 0);
		$node->content['invoice_items']['#value'] =  $invoice_items;
		$node->content['invoice_items']['#weight'] = -7;

		
		//Inject client details
		if (!empty($node->field_to[0][nid]) && is_numeric($node->field_to[0][nid])) {
		    $node_client = node_load($node->field_to[0][nid]);
		    $node->content['client'] = array (
			'#value' => theme('client_details',$node_client ),
			'#title' => 'To',
			'#type' => 'fieldset',
			'#weight' => -8,
			'#attributes' => array( 'class' => 'client-details'),
		    );	
		}
		
		if (!empty($node->field_from[0][nid]) && is_numeric($node->field_from[0][nid])) {
		    $node_from = node_load($node->field_from[0][nid]);
		    $node->content['issuer'] = array (
			'#value' => theme('invoice_issuer',$node_from ),
			'#title' => 'From',
			'#type' => 'fieldset',
			'#weight' => -9,
			'#attributes' => array( 'class' => 'invoice-issuer'),
		    );	
		}

	}

    // Autogenerate number
    if ($node->form_id == 'invoice_node_form' and $node->type == 'invoice') {
		switch ($op) {
		    case 'insert': // after inserting invoice, increase id
				if($node->field_pro_forma[0]['value'] == 'yes'){
					$inv_no = variable_get('invoice_next_pf_id',1);
					variable_set('invoice_next_pf_id',++$inv_no);
					
				} else {
					$inv_no = variable_get('invoice_next_id',invoices_get_last_id_from_db());
					variable_set('invoice_next_id',++$inv_no);
				}
		    break;				 
		}
    }
    
    if ($node->type == 'invoice') {
    	switch ($op) {
		    case 'insert':
		    case 'update':
		    case 'submit':
		    	
		    	if($node->field_pro_forma[0]['value'] == 'yes') { // value 'yes' - after optionwidgets patch
		    		$node->title = "Pro Forma Invoice Number " .$node->field_invoice_number[0]['value'];
		    	} else {
		    		$node->title = "Invoice Number " .$node->field_invoice_number[0]['value'];
		    	}
		     
		    break;				 
		}
    }

}

function theme_bank_account($node_bank_account){

 	$node = node_build_content($node_bank_account);
 	$content = drupal_render($node->content);
 	$o .= $content ;	
	return $o;
}

function theme_client_details($node){
 	$node = node_build_content($node);
 	$content = drupal_render($node->content);
 	$o .= $content ;	
	return $o;
}


function theme_invoice_issuer($node){
 	$node = node_build_content($node);
 	$content = drupal_render($node->content);
 	$o .= $content ;	
	return $o;
}



/**
 * hook_link implementation
 */
function invoices_link($type, &$node){
	if ($type == 'node' and $node->type == 'invoice') {	
		if (user_access('create invoice_item content')){ 
			$links['new_invoice_item']['title'] = 'New invoice item';
			$links['new_invoice_item']['href'] = 'node/add/invoice-item/'. $node->nid;
//			$links['new_invoice_item']['query'] = 'parent_invoice=' . $node->nid;
		}
	}
	return $links;
}


<?php
/**
 * Author: Bernard Szlachta
 * Company: NobleProg Limited
 * URL: www.nobleprog.co.uk
 * bernard.szlachta (at) NobleProg.co.uk
 */

function invoices_views_default_views() {
	
/* Invoice Items	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*/	
  $view = new stdClass();
  $view->name = 'invoice_items';
  $view->description = '';
  $view->access = array ();
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = '';
  $view->page_header = '';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = '';
  $view->page_empty_format = '1';
  $view->page_type = 'table';
  $view->url = 'invoice_items';
  $view->use_pager = TRUE;
  $view->nodes_per_page = '10000';
  $view->block = TRUE;
  $view->block_title = '';
  $view->block_header = '';
  $view->block_header_format = '0';
  $view->block_footer = '';
  $view->block_footer_format = '0';
  $view->block_empty = '';
  $view->block_empty_format = '1';
  $view->block_type = 'table';
  $view->nodes_per_block = '10';
  $view->block_more = FALSE;
  $view->block_use_page_header = FALSE;
  $view->block_use_page_footer = FALSE;
  $view->block_use_page_empty = FALSE;
  $view->sort = array (
  );
  $view->argument = array (
    array (
      'type' => 'content: field_parent_invoice',
      'argdefault' => '1',
      'title' => '',
      'options' => '',
      'wildcard' => '',
      'wildcard_substitution' => '',
    ),
  );
  $view->field = array (
    array (
      'tablename' => 'node',
      'field' => 'title',
      'label' => 'Name',
      'handler' => 'views_handler_field_nodelink',
      'options' => 'link',
    ),
    array (
      'tablename' => 'node_data_field_unit',
      'field' => 'field_unit_value',
      'label' => 'Unit',
      'handler' => 'content_views_field_handler_group',
      'options' => 'default',
    ),
    array (
      'tablename' => 'node_data_field_quantity',
      'field' => 'field_quantity_value',
      'label' => 'Quantity',
      'handler' => 'content_views_field_handler_ungroup',
      'options' => 'default',
    ),
    array (
      'tablename' => 'node_data_field_unit_price',
      'field' => 'field_unit_price_value',
      'label' => 'Unit Price',
      'handler' => 'content_views_field_handler_group',
      'options' => 'default',
    ),
    array (
      'tablename' => 'node_data_field_item_price',
      'field' => 'field_item_price_value',
      'label' => 'Price',
      'handler' => 'content_views_field_handler_ungroup',
      'options' => 'plain',
    ),
  );
  $view->filter = array (
    array (
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
  0 => 'invoice_item',
),
    ),
  );
  $view->exposed_filter = array (
  );
  $view->requires = array(node, node_data_field_unit, node_data_field_quantity, node_data_field_unit_price, node_data_field_item_price);
  $views[$view->name] = $view;



	
/* Invoice list		*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*/
  $view = new stdClass();
  $view->name = 'Invoices';
  $view->description = 'List, sorts and filters invoices';
  $view->access = array (
  0 => '6',
);
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = '';
  $view->page_header = '<a href="/node/add/invoice">New invoice</a>';
  $view->page_header_format = '3';
  $view->page_footer = '<a href="/node/add/invoice">New invoice</a>';
  $view->page_footer_format = '3';
  $view->page_empty = '';
  $view->page_empty_format = '3';
  $view->page_type = 'table';
  $view->url = 'invoices';
  $view->use_pager = TRUE;
  $view->nodes_per_page = '30';
  $view->sort = array (
  );
  $view->argument = array (
  );
  $view->field = array (
    array (
      'tablename' => 'node_data_field_pro_forma',
      'field' => 'field_pro_forma_value',
      'label' => 'Pro Forma',
      'handler' => 'content_views_field_handler_group',
      'options' => 'default',
    ),
    array (
      'tablename' => 'node_data_field_invoice_number',
      'field' => 'field_invoice_number_value',
      'label' => 'No',
      'handler' => 'content_views_field_handler_group',
      'options' => 'default',
    ),
    array (
      'tablename' => 'node',
      'field' => 'created',
      'label' => '',
      'handler' => 'views_handler_field_date_small',
      'sortable' => '1',
    ),
    array (
      'tablename' => 'node',
      'field' => 'changed',
      'label' => '',
      'handler' => 'views_handler_field_date_small',
      'sortable' => '1',
    ),
    array (
      'tablename' => 'node_data_field_to',
      'field' => 'field_to_nid',
      'label' => 'Client',
      'handler' => 'content_views_field_handler_group',
      'options' => 'default',
    ),
    array (
      'tablename' => 'node_data_field_issue_date',
      'field' => 'field_issue_date_value',
      'label' => 'Issue Date',
      'handler' => 'content_views_field_handler_group',
      'sortable' => '1',
      'defaultsort' => 'DESC',
      'options' => 'default',
    ),
    array (
      'tablename' => 'node_data_field_from',
      'field' => 'field_from_nid',
      'label' => 'From',
      'handler' => 'content_views_field_handler_group',
      'options' => 'default',
    ),
    array (
      'tablename' => 'node',
      'field' => 'view',
      'label' => '',
    ),
    array (
      'tablename' => 'node_data_field_paid',
      'field' => 'field_paid_value',
      'label' => 'Paid',
      'handler' => 'content_views_field_handler_group',
      'options' => 'default',
    ),
  );
  $view->filter = array (
    array (
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
  0 => 'invoice',
),
    ),
    array (
      'tablename' => 'node_data_field_paid',
      'field' => 'field_paid_value_default',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
  0 => 'No',
  1 => 'Yes',
),
    ),
    array (
      'tablename' => 'node_data_field_to',
      'field' => 'field_to_nid_default',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
  0 => '0',
),
    ),
    array (
      'tablename' => 'node_data_field_from',
      'field' => 'field_from_nid_default',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
  0 => '0',
),
    ),
    array (
      'tablename' => 'node_data_field_pro_forma',
      'field' => 'field_pro_forma_value_like',
      'operator' => '=',
      'options' => '',
      'value' => '',
    ),
  );
  $view->exposed_filter = array (
    array (
      'tablename' => 'node_data_field_paid',
      'field' => 'field_paid_value_default',
      'label' => 'Paid',
      'optional' => '1',
      'is_default' => '0',
      'operator' => '1',
      'single' => '1',
    ),
    array (
      'tablename' => 'node_data_field_to',
      'field' => 'field_to_nid_default',
      'label' => 'Client',
      'optional' => '1',
      'is_default' => '0',
      'operator' => '1',
      'single' => '1',
    ),
    array (
      'tablename' => 'node_data_field_from',
      'field' => 'field_from_nid_default',
      'label' => 'Issuer',
      'optional' => '1',
      'is_default' => '0',
      'operator' => '1',
      'single' => '1',
    ),
    array (
      'tablename' => 'node_data_field_pro_forma',
      'field' => 'field_pro_forma_value_like',
      'label' => 'ProForma',
      'optional' => '0',
      'is_default' => '0',
      'operator' => '1',
      'single' => '0',
    ),
  );
  $view->requires = array(node_data_field_pro_forma, node_data_field_invoice_number, node, node_data_field_to, node_data_field_issue_date, node_data_field_from, node_data_field_paid);
  $views[$view->name] = $view;
  
  
  
  
  
  
  
    $view = new stdClass();
  $view->name = 'parent_invoices';
  $view->description = 'It appears on invoice_item form';
  $view->access = array (
);
  $view->view_args_php = '';
  $view->page = FALSE;
  $view->page_title = '';
  $view->page_header = '';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = '';
  $view->page_empty_format = '1';
  $view->page_type = 'node';
  $view->url = '';
  $view->use_pager = TRUE;
  $view->nodes_per_page = '30';
  $view->sort = array (
    array (
      'tablename' => 'node',
      'field' => 'created',
      'sortorder' => 'DESC',
      'options' => 'normal',
    ),
  );
  $view->argument = array (
  );
  $view->field = array (
    array (
      'tablename' => 'node',
      'field' => 'title',
      'label' => '',
      'handler' => 'views_handler_field_nodelink',
      'options' => 'link',
    ),
  );
  $view->filter = array (
    array (
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
  0 => 'invoice',
),
    ),
  );
  $view->exposed_filter = array (
  );
  $view->requires = array(node);
  $views[$view->name] = $view;
  
  
  
  
	
	
  return $views;
	
}




/**
 * The hook_views_pre_view implementation
 */
function invoices_views_pre_view(&$view, &$items) {
	if($view->name == 'invoice_items'){
		$sum = 0;
		foreach ($items as $item){
			$sum += $item->node_data_field_item_price_field_item_price_value; 
		}
		
		$view->page_footer_format = 2;
		$view->page_footer =  theme('invoice_items_sumary',$sum, $sum);
	}
}

function theme_invoice_items_sumary($amount,$balance_due){
	$o = '<div class="invoice_amount">Amount: ' . $amount. '</div>';
	$o .= '<div class="balance_due">Balance Due: ' . $balance_due. '</div>';
	return $o;
}
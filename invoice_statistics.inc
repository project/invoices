<?php
/**
 * Author: Bernard Szlachta
 * Company: NobleProg Limited
 * URL: www.nobleprog.co.uk
 * bernard.szlachta (at) NobleProg.co.uk
 */


/**
 * Sumarizes the invoice statistics
 */
function invoices_landing_page(){
	$companies = node_get_nodes('company',1);
	foreach ($companies as $company){
		$statistics = invoices_stat_sums($company->nid);
		$o .= theme('invoice_statistics_companies',$company,$statistics);
	}
	
	return $o;
}

function theme_invoice_statistics_companies($company,$statistics){
	$o .= '<h2>' .  $company->title . '</h2>';
	$o .= theme('invoice_statistics',$statistics);
	return $o;
	
}
function theme_invoice_statistics($statistics){	
		return theme('table',array("Name","Value"),$statistics);
}

/**
 * Generats statistics for each company
 * @par integer company_id
 * @return array with statists
 */
function invoices_stat_sums($company_id = 0){
	$invoice_items = node_get_nodes('invoice_item',1);
	$invoices 	   = node_get_nodes('invoice',1);

	$sum = 0;
	$sum_paid =0;
	foreach($invoice_items as  $key => $item ){
			
		$invoice = $invoices[$item->field_parent_invoice[0]['nid']];
		if($invoice->field_pro_forma[0]['value'] != 'yes'){
			if($company_id == 0 or $invoice->field_from[0]['nid'] == $company_id){
				$sum += $item->field_item_price[0]['value'];
				
				if($invoice->field_paid[0]['value'] == 'Yes'){
					
					$sum_paid += $item->field_item_price[0]['value'];
				}
			}
		}
	}
	$statistics['item_total'] = 		array('label' => "Total", 			'value' => $sum);
	$statistics['item_total_paid'] = 	array('label' => "Total Paid", 		'value' => $sum_paid);
	$statistics['item_total_notpaid'] = array('label' => "Total Not Paid", 	'value' => $sum - $sum_paid);;
	return $statistics;
}

/**
 * Returns nodes of specific types and status
 */
function node_get_nodes($type,$status){
	$result = db_query("SELECT nid FROM {node} WHERE type = '%s' AND status = %d",
														$type,			$status);
	while($row =  db_fetch_object($result)){
		$node = node_load($row->nid);
		$nodes[$row->nid] = $node;
	}
	return $nodes;
}




/**
 * Implementation of hook_block().
 **/
function invoices_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      $blocks[0]['info'] = t('Invoices');
      return $blocks;

    case 'view':
        switch ($delta) {
          case 0:
	       if(user_access('view invoice statistics')){
	           
	           $l[] = l('Invoice list','invoices');
	       	   $l[] = l('Validate invoices','admin/nobleprog/invoices');
	           $l[] = l('Invoice statistcs','invoice_stats');
	       }
	        
	        if(user_access('create invoice content')) { 
	            $l[] = l('New Invoice','node/add/invoice');		
	        }

	        if(user_access('create client content')) { 
	            $l[] = l('New client','node/add/client');		
	        }

	        if(user_access('create company content')) { 
	            $l[] = l('New company','node/add/company');		
	        }

	        if(user_access('create bank_account content')) { 
	            $l[] = l('New bank account','node/add/bank-account');		
	        }

	        $block['subject'] = "Invoices";
	        $block['content'] = theme('item_list',$l); 
        }

        return $block;
      }
}
